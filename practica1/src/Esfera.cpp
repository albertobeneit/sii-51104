// Esfera.cpp: implementation of the Esfera class.
//
//
//Practica 2 - Alberto Beneit - 51104
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	//añadimos movimiento
	centro=centro+velocidad*t;
	
	//Practica 2 hacemos que se haga pequeña la bola
	radio-=0.05*t;
	if(radio<0.05) 
	{
		radio=0.5; 
	}
	

}

